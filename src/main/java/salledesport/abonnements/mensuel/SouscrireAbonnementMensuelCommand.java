package salledesport.abonnements.mensuel;

import salledesport.abonnements.FormuleChoisie;

public class SouscrireAbonnementMensuelCommand {
    public int idAbonne;
    public FormuleChoisie formuleChoisie;

    public SouscrireAbonnementMensuelCommand(int idAbonne, FormuleChoisie formuleChoisie) {
        this.idAbonne = idAbonne;
        this.formuleChoisie = formuleChoisie;
    }

    public SouscrireAbonnementMensuelCommand() {

    }
}
