package salledesport.abonnements.mensuel;

import salledesport.abonnements.AbonnementAggregate;
import salledesport.UuidGenerator;

public class SouscrireAbonnementMensuelCommandHandler {

    private final UuidGenerator uuidGenerator;

    public SouscrireAbonnementMensuelCommandHandler(UuidGenerator uuidGenerator) {
        this.uuidGenerator = uuidGenerator;

    }

    public AbonnementMensuelSouscritEvent handle(SouscrireAbonnementMensuelCommand command) {
        return new AbonnementAggregate().souscrireAbonnementMensuel(uuidGenerator.nextUuid(),command.idAbonne,command.formuleChoisie.prixDeBase);
    }
}
