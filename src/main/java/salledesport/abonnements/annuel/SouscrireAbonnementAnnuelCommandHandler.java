package salledesport.abonnements.annuel;

import salledesport.abonnements.AbonnementAggregate;
import salledesport.UuidGenerator;

public class SouscrireAbonnementAnnuelCommandHandler {

    private UuidGenerator uuidGenerator;

    public SouscrireAbonnementAnnuelCommandHandler() {
        this.uuidGenerator = new UuidGenerator();
    }

    public SouscrireAbonnementAnnuelCommandHandler(UuidGenerator uuidGenerator) {
        this.uuidGenerator = uuidGenerator;
    }

    public AbonnementAnnuelSouscritEvent handle(SouscrireAbonnementAnnuelCommand command) {
        AbonnementAggregate abonnementAggregate = new AbonnementAggregate();
        return abonnementAggregate.souscrireAbonnementAnnuel(this.uuidGenerator.nextUuid(), command.idAbonne,command.formuleChoisie.prixDeBase) ;
    }
}
