package salledesport.abonnements.annuel;

import salledesport.abonnements.FormuleChoisie;

public class SouscrireAbonnementAnnuelCommand {

    public Integer idAbonne;
    public FormuleChoisie formuleChoisie;
}
