package salledesport.abonnements;

import salledesport.abonnements.annuel.AbonnementAnnuelSouscritEvent;
import salledesport.abonnements.mensuel.AbonnementMensuelSouscritEvent;

public class AbonnementAggregate {
    private Integer idAbonne;
    private Integer prixDeBAse;
    private Integer isAbonnement;
    private PeriodeAbonnement periode;

    public AbonnementAnnuelSouscritEvent souscrireAbonnementAnnuel(Integer idAbonnement, Integer idAbonne, Integer prixDeBase) {
        this.isAbonnement = idAbonnement;
        this.idAbonne = idAbonne;
        this.prixDeBAse = prixDeBase;
        this.periode = PeriodeAbonnement.ANNUEL;
        return new AbonnementAnnuelSouscritEvent(idAbonnement);
    }

    public AbonnementMensuelSouscritEvent souscrireAbonnementMensuel(Integer idAbonnement, Integer idAbonne, Integer prixDeBase) {
        this.isAbonnement = idAbonnement;
        this.idAbonne = idAbonne;
        this.prixDeBAse = prixDeBase;
        this.periode = PeriodeAbonnement.MENSUEL;
        return new AbonnementMensuelSouscritEvent(idAbonnement);
    }
}
