package salledesport.adminformule;

public class FormuleAnnuelleCreeeEvent {
    private Integer idFormule;

    private FormuleAnnuelleCreeeEvent(Integer idFormule) {
        this.idFormule = idFormule;
    }

    public static FormuleAnnuelleCreeeEvent instantiate(Integer id) {
        return new FormuleAnnuelleCreeeEvent(id);
    }

    public Integer getIdFormule() {
        return idFormule;
    }
}
