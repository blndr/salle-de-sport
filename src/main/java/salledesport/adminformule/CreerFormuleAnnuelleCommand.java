package salledesport.adminformule;

public class CreerFormuleAnnuelleCommand {
    public final String nom;
    public final int prix;

    public CreerFormuleAnnuelleCommand(String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
    }
}
