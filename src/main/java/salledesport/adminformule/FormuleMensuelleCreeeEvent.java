package salledesport.adminformule;

public class FormuleMensuelleCreeeEvent {

    private int idFormule;

    private FormuleMensuelleCreeeEvent(int idFormule) {
        this.idFormule = idFormule;
    }

    public static FormuleMensuelleCreeeEvent instantiate(int id) {
        return new FormuleMensuelleCreeeEvent(id);
    }

    public int getIdFormule() {
        return idFormule;
    }
}
