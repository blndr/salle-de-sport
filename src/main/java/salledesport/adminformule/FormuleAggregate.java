package salledesport.adminformule;

public class FormuleAggregate {

    private String nom;
    private int prix;
    private Integer id;

    public FormuleAggregate(Integer uuid, String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
        this.id = uuid;
    }

    public FormuleMensuelleCreeeEvent creerNouvelleFormuleMensuelle() {
        FormuleMensuelleCreeeEvent event = FormuleMensuelleCreeeEvent.instantiate(this.id);
        return event;
    }

    public FormuleAnnuelleCreeeEvent creerNouvelleFormuleAnnuelle() {
        FormuleAnnuelleCreeeEvent event = FormuleAnnuelleCreeeEvent.instantiate(this.id);
        return event;
    }
}
