package salledesport.adminformule;

import salledesport.UuidGenerator;

public class CreerFormuleMensuelleCommandHandler {

    private UuidGenerator uuidGenerator;

    public CreerFormuleMensuelleCommandHandler() {
        this.uuidGenerator = new UuidGenerator();
    }

    public CreerFormuleMensuelleCommandHandler(UuidGenerator uuidGenerator) {
        this.uuidGenerator = uuidGenerator;
    }

    public FormuleMensuelleCreeeEvent handle(CreerFormuleMensuelleCommand commande) {
        Integer uuid = this.uuidGenerator.nextUuid();
        FormuleAggregate formule = new FormuleAggregate(uuid, commande.nom, commande.prix);
        FormuleMensuelleCreeeEvent event = formule.creerNouvelleFormuleMensuelle();
        return event;
    }
}
