package salledesport.adminformule;

public class CreerFormuleMensuelleCommand {
    public String nom;
    public int prix;

    public CreerFormuleMensuelleCommand(String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
    }
}
