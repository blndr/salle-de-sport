package salledesport.adminformule;

import salledesport.UuidGenerator;

public class CreerFormuleAnnuelleCommandHandler {

    private UuidGenerator uuidGenerator;

    public CreerFormuleAnnuelleCommandHandler() {
        this.uuidGenerator = new UuidGenerator();
    }

    public CreerFormuleAnnuelleCommandHandler(UuidGenerator uuidGenerator) {
        this.uuidGenerator = uuidGenerator;
    }

    public FormuleAnnuelleCreeeEvent handle(CreerFormuleAnnuelleCommand commande) {
        Integer uuid = this.uuidGenerator.nextUuid();
        FormuleAggregate formule = new FormuleAggregate(uuid, commande.nom, commande.prix);
        FormuleAnnuelleCreeeEvent event = formule.creerNouvelleFormuleAnnuelle();
        return event;
    }
}
