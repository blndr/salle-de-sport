package salledesport.adminformule;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FormuleAggregateTest {

    @Test
    public void retourneUnEvenementFormationMensuelleContenantIdFormuleCree() {
        // given
        FormuleAggregate formuleAggregate = new FormuleAggregate(1, "Sport+", 100);

        // when
        FormuleMensuelleCreeeEvent event = formuleAggregate.creerNouvelleFormuleMensuelle();

        // then
        assertThat(event.getIdFormule()).isEqualTo(1);
    }

    @Test
    public void retourneUnEvenementFormationAnnuelleContenantIdFormuleCree() {
        // given
        FormuleAggregate formuleAggregate = new FormuleAggregate(1, "Sport+", 100);

        // when
        FormuleAnnuelleCreeeEvent event = formuleAggregate.creerNouvelleFormuleAnnuelle();

        // then
        assertThat(event.getIdFormule()).isEqualTo(1);
    }
}
