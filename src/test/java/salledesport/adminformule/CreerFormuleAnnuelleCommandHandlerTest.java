package salledesport.adminformule;

import org.junit.Test;
import salledesport.UuidGenerator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreerFormuleAnnuelleCommandHandlerTest {
    @Test
    public void retourneUnEvenementAvecIdFormuleCreee() {
        // given
        UuidGenerator uuidGenerator = mock(UuidGenerator.class);
        when(uuidGenerator.nextUuid()).thenReturn(23435);

        CreerFormuleAnnuelleCommandHandler command = new CreerFormuleAnnuelleCommandHandler(uuidGenerator);

        // when
        FormuleAnnuelleCreeeEvent event = command.handle(new CreerFormuleAnnuelleCommand("Sport+", 100));

        // then
        assertThat(event.getIdFormule()).isEqualTo(23435);
    }
}
