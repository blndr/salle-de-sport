package salledesport.adminformule;


import org.junit.Test;
import salledesport.UuidGenerator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreerFormuleMensuelleCommandHandlerTest {

    @Test
    public void retourneUnEvenementAvecIdFormuleCreee() {
        // given
        UuidGenerator uuidGenerator = mock(UuidGenerator.class);
        when(uuidGenerator.nextUuid()).thenReturn(768876);
        CreerFormuleMensuelleCommandHandler command = new CreerFormuleMensuelleCommandHandler(uuidGenerator);

        // when
        FormuleMensuelleCreeeEvent event = command.handle(new CreerFormuleMensuelleCommand("Sport+", 100));

        // then
        assertThat(event.getIdFormule()).isEqualTo(768876);
    }
}
