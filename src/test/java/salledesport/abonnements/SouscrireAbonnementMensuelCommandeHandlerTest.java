package salledesport.abonnements;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import salledesport.abonnements.mensuel.AbonnementMensuelSouscritEvent;
import salledesport.abonnements.mensuel.SouscrireAbonnementMensuelCommand;
import salledesport.abonnements.mensuel.SouscrireAbonnementMensuelCommandHandler;
import salledesport.UuidGenerator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SouscrireAbonnementMensuelCommandeHandlerTest {
    @Test
    public void test() {
        // given
        SouscrireAbonnementMensuelCommand command = new SouscrireAbonnementMensuelCommand();
        command.idAbonne = 1234;
        command.formuleChoisie = new FormuleChoisie();
        command.formuleChoisie.prixDeBase = 200;

        UuidGenerator uuidGenerator = mock(UuidGenerator.class);
        when(uuidGenerator.nextUuid()).thenReturn(3256);

        SouscrireAbonnementMensuelCommandHandler handler = new SouscrireAbonnementMensuelCommandHandler(uuidGenerator);

        // when
        AbonnementMensuelSouscritEvent event = handler.handle(command);

        // then
        Assertions.assertThat(event.idAbonnement).isEqualTo(3256);
    }
}