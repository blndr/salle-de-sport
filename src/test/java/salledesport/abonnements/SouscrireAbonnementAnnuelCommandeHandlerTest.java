package salledesport.abonnements;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import salledesport.abonnements.annuel.AbonnementAnnuelSouscritEvent;
import salledesport.abonnements.annuel.SouscrireAbonnementAnnuelCommand;
import salledesport.abonnements.annuel.SouscrireAbonnementAnnuelCommandHandler;
import salledesport.UuidGenerator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SouscrireAbonnementAnnuelCommandeHandlerTest {

    @Test
    public void test() {
        // given
        SouscrireAbonnementAnnuelCommand command = new SouscrireAbonnementAnnuelCommand();
        command.idAbonne = 1234;
        command.formuleChoisie = new FormuleChoisie();
        command.formuleChoisie.prixDeBase = 200;

        UuidGenerator uuidGenerator = mock(UuidGenerator.class);
        when(uuidGenerator.nextUuid()).thenReturn(3256);

        SouscrireAbonnementAnnuelCommandHandler handler = new SouscrireAbonnementAnnuelCommandHandler(uuidGenerator);

        // when
        AbonnementAnnuelSouscritEvent event = handler.handle(command);

        // then
        Assertions.assertThat(event.idAbonnement).isEqualTo(3256);

    }
}
